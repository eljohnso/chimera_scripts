import tkinter as tk
import pyjapc
import numpy as np
from madxtools.transfer_function import *

japc = pyjapc.PyJapc(noSet=False)
japc.rbacLogin()

root = tk.Tk()
font_size = 'Arial 12'  # You can modify this to increase/decrease the font size
root.title(f"Energy GUI")

plateau_number = 0
second_plateau_index = 1

def set_bfield():
    selector = selector_entry.get()
    E_cin_per_nucleon = float(ekin_entry.get())

    if validate_float(E_cin_per_nucleon):

        # Ion properties
        A = 208.0
        Z = 82.0
        N = 126.0
        charge = 54.0
        m_proton_GeV = 0.93828
        m_neutron_GeV = 0.93957
        m_electron_GeV = 0.000511
        m_u_GeV = 0.9315
        mass_defect_GeV = Z * m_proton_GeV + N * m_neutron_GeV + (Z - charge) * m_electron_GeV - A * m_u_GeV
        E_0 = Z * m_proton_GeV + N * m_neutron_GeV - mass_defect_GeV
        p = E_0 * np.sqrt((((E_cin_per_nucleon * A) / E_0) + 1) ** 2 - 1)
        rho = 70.0789
        B = 3.3356 * p / (rho * charge) * 10000

        new_bfield = B

        japc.setSelector(selector)
        bfield = japc.getParam("PR.MPC/REF.PPPL")
        bfield["REF4"][plateau_number] = new_bfield

        try:
            if new_bfield < 4250:
                bfield["ACCELERATION1"][second_plateau_index] = 300000
                bfield["ACCELERATION3"][second_plateau_index] = -300000
                bfield["RATE2"][second_plateau_index] = 22000
                japc.setParam("PR.MPC/REF.PPPL", bfield, checkDims=False)
            if new_bfield >= 4250:
                print(f"new bfield is higher than 4250, = {new_bfield}")
                bfield["ACCELERATION1"][second_plateau_index] = -300000
                bfield["ACCELERATION3"][second_plateau_index] = 300000
                bfield["RATE2"][second_plateau_index] = -22000
                japc.setParam("PR.MPC/REF.PPPL", bfield, checkDims=False)
        except Exception as e:
            print('Ignored pyjapc error')
            print(e)

    else:
        print("Error: Ekin out of range or non-float")

def read_bfield():
    selector = selector_entry.get()

    japc.setSelector(selector)
    bfield = japc.getParam("PR.MPC/REF.PPPL#REF4")

    bfield_label['text'] = f"Current B-field = {round(float(bfield[plateau_number]),1)} G"
    Ekin = (193.737692/208)*(np.sqrt(((1/((3.3356/(70.0789*54)*10000)/bfield[plateau_number]))/193.737692)**2+1)-1)
    ekin_label['text'] = f"Ekin = {round(Ekin,3)} GeV/u"

    rho = 70.0789
    Brho = rho*bfield[plateau_number]/10000
    Brho_label['text'] = f"Brho = {round(Brho,3)}, Brho fully stripped = {round(Brho * 54/82,3)} "

def set_magnets():
    alpha = float(alpha_entry.get())
    selector = selector_entry.get()

    japc.setSelector(selector)
    bfield = japc.getParam("PR.MPC/REF.PPPL#REF4")

    rho = 70.0789
    Brho = rho*bfield[plateau_number]/10000

    dump_bend = -get_current_dipole(alpha, "MCB", Brho)*54/82

    japc.setParam("F61.BHZ01.DUMP.A/REF.PULSE.AMPLITUDE#VALUE", dump_bend)
    japc.setParam("F61.BHZ01.DUMP.B/REF.PULSE.AMPLITUDE#VALUE", dump_bend)

    print(f'Setting dump bend current to = {round(dump_bend,3)} [A]')

def validate_float(value):
    try:
        float_value = float(value)
        return 0.4 <= float_value <= 2.7
    except ValueError:
        return False

selector_label = tk.Label(root, text="Selector:", font=font_size)
selector_label.grid(row=0, column=0)
selector_entry = tk.Entry(root, font=font_size)
selector_entry.insert(0, "CPS.USER.MD3")  # Default value
selector_entry.grid(row=0, column=1)


ekin_label = tk.Label(root, text="New Ekin (G):", font=font_size)
ekin_label.grid(row=1, column=0)
ekin_entry = tk.Entry(root, font=font_size)
ekin_entry.grid(row=1, column=1)

set_button = tk.Button(root, text="Set Ekin", command=set_bfield, font=font_size)
set_button.grid(row=2, column=1)

read_button = tk.Button(root, text="Read B-field", command=read_bfield, font=font_size)
read_button.grid(row=2, column=0)

bfield_label = tk.Label(root, text="", font=font_size)
bfield_label.grid(row=3, column=0, columnspan=2)

ekin_label = tk.Label(root, text="", font=font_size)
ekin_label.grid(row=4, column=0, columnspan=2)

Brho_label = tk.Label(root, text="", font=font_size)
Brho_label.grid(row=5, column=0, columnspan=2)

default_alpha = 0.047
alpha_label = tk.Label(root, text="Alpha:", font=font_size)
alpha_label.grid(row=6, column=1)
alpha_entry = tk.Entry(root, font=font_size)
alpha_entry.grid(row=6, column=2)
alpha_entry.insert(0, default_alpha)


set_magnet_button = tk.Button(root, text="Set Dump Magnets", command=set_magnets, font=font_size)
set_magnet_button.grid(row=6, column=0)

# read_bfield()
root.mainloop()
