import tkinter as tk
from tkinter.ttk import Progressbar
from tkinter import ttk
import pyjapc
import itertools
import datetime
from madxtools.transfer_function import *
from madxtools.math import *
from energy_GUI_config import *
from PIL import Image, ImageTk

japc = pyjapc.PyJapc(noSet=True)
# japc.rbacLogin()

root = tk.Tk()
root.title(window_title)
hearts_image = ImageTk.PhotoImage(Image.open('images/Logo_Hearts_cropped.png'))
root.wm_iconphoto(False, hearts_image)

def set_bfield():
    global started_subscription
    E_cin_per_nucleon = float(ekin_entry.get())
    if started_subscription == False:
        if validate_float(E_cin_per_nucleon):
            new_bfield = pb_ion_bfield(E_cin_per_nucleon)
            japc.setSelector(selector_entry.get())
            bfield = japc.getParam("PR.MPC/REF.PPPL")
            bfield["REF4"][plateau_number] = round(new_bfield,3)
            try:
                japc.setParam("PR.MPC/REF.PPPL", bfield, checkDims=False)
                if checkbox_var.get():
                    set_magnets()  # Dump magnets
                read_bfield()
            except Exception as e:
                print('Ignored pyjapc error')
                print(e)
        else:
            print("Error: Ekin out of range or non-float")
            status_label.config(text=f"Error: Ekin out of range or non-float")
    else:
        print("Can't manually set during scan")
        status_label.config(text=f"Error: Can't manually set during scan")

def read_bfield():
    japc.setSelector(selector_entry.get())
    bfield = japc.getParam("PR.MPC/REF.PPPL#REF4")
    bfield_label['text'] = f"Current B-field = {round(float(bfield[plateau_number]), 1)} [G]"
    Ekin, Brho = ekin_brho_from_bfield(bfield[plateau_number])
    ekin_label['text'] = f"Ekin = {round(Ekin,3)} [GeV/u]"
    Brho_label['text'] = f"Brho = {round(Brho,3)} [GeV/c]"

def set_magnets():
    alpha = float(alpha_entry.get())
    japc.setSelector(selector_entry.get())
    bfield = japc.getParam("PR.MPC/REF.PPPL#REF4")
    Ekin, Brho = ekin_brho_from_bfield(bfield[plateau_number])
    dump_bend = -get_current_dipole(alpha, "MCB", Brho)*54/82
    japc.setParam("F61.BHZ01.DUMP.A/REF.PULSE.AMPLITUDE#VALUE", dump_bend)
    japc.setParam("F61.BHZ01.DUMP.B/REF.PULSE.AMPLITUDE#VALUE", dump_bend)
    print(f'Setting dump bend current to = {round(dump_bend,3)} [A]')

def validate_float(value):
    try:
        float_value = float(value)
        return min_input_ekin <= float_value <= max_input_ekin
    except ValueError:
        return False

def validate_gain_float(value):
    try:
        float_value = float(value)
        return min_input_gain <= float_value <= max_input_gain
    except ValueError:
        return False

def set_bfield_scan(E_cin_per_nucleon):
    if validate_float(E_cin_per_nucleon):
        new_bfield = pb_ion_bfield(E_cin_per_nucleon)
        print(f"Setting to {round(new_bfield,1)} [G]")

        japc.setSelector(selector_entry.get())
        bfield = japc.getParam("PR.MPC/REF.PPPL")
        bfield["REF4"][plateau_number] = new_bfield
        try:
            japc.setParam("PR.MPC/REF.PPPL", bfield, checkDims=False)
        except Exception as e:
            print('Ignored pyjapc error')
            print(e)
            stop_subscriptions()
    else:
        print("Error: Ekin out of range or non-float")

i=0
ekin_array = None
def myCallback(parameterName, newValue):
    global i
    global ekin_array
    global started_subscription
    global start_scan_time

    progress = (i + 1) / len(ekin_array) * 100
    progress_label.config(text=f"Progress: {int(progress)}%")
    status_label.config(text=f"Status: Scan in progress")
    progress_bar["value"] = progress

    # Estimate the time between callbacks and calculate expected end time
    if i > 0:
        time_elapsed = datetime.datetime.now() - start_scan_time
        expected_total_time = time_elapsed / (i / len(ekin_array))
        expected_end_time = start_scan_time + expected_total_time
        expected_end_time_str = expected_end_time.strftime("%H:%M:%S")
        expected_end_label.config(text=f"Expected End Time: {expected_end_time_str}")

    print(f"Setting Ekin to {round(ekin_array[i],3)} [GeV/u]")
    print(f"Scan {i}/{len(ekin_array)}")

    set_bfield_scan(ekin_array[i])
    if checkbox_var.get():
        set_magnets() # Dump magnets
    read_bfield()
    print("Done")

    i+=1
    if i==len(ekin_array):
        stop_subscriptions()

started_subscription = False
start_scan_time = None
def start_subscriptions():
    global ekin_array
    global started_subscription
    global start_scan_time
    japc.setSelector(selector_entry.get())
    if started_subscription == False:
        started_subscription = True
        start_scan_time = datetime.datetime.now()
        status_label.config(text=f"Scan in progress")
        expected_end_label.config(text=f"Expected End Time: Calculating...")
        ekin_array = np.linspace(float(start_ekin_entry.get()), float(end_ekin_entry.get()), int(steps_ekin_entry.get()))
        ekin_array = list(itertools.chain.from_iterable(itertools.repeat(x, int(repetition_ekin_entry.get())) for x in ekin_array))
        japc.subscribeParam("PR.BCT/HotspotIntensity#dcBefEje2", myCallback)
        japc.startSubscriptions()
        print('Started subscription')
    else:
        print("Can't start another subscription")
        status_label.config(text=f"Error: Can't start another subscription")

def stop_subscriptions():
    global i
    global started_subscription
    global ekin_array
    if (i > 0) and (i < len(ekin_array)): # Before the scan finished
        i = 0
        status_label.config(text=f"Status: Scan Cancelled")
        progress = (i) / len(ekin_array) * 100
        progress_label.config(text=f"Progress: {int(progress)}%")
        progress_bar["value"] = progress
        expected_end_label.config(text=f"Expected End Time:")
    if (i > 0) and (i == len(ekin_array)):
        status_label.config(text=f"Status: Scan Completed")
        progress = (i) / len(ekin_array) * 100
        progress_label.config(text=f"Progress: {int(progress)}%")
        progress_bar["value"] = progress
        expected_end_label.config(text=f"Expected End Time: ")
    else:
        status_label.config(text=f"Status: Scan Cancelled")
    i = 0
    print("SCAN COMPLETED")
    japc.stopSubscriptions()
    japc.clearSubscriptions()
    started_subscription = False

def set_gain():
    global started_subscription
    gain = float(gain_entry.get())
    if validate_gain_float(gain):
        japc.setSelector(selector_entry.get())
        try:
            print(f"Setting gain to {gain}")
            japc.setParam("PR.BQL72/Setting#exAmplitudeH", gain)
        except Exception as e:
            print('Ignored pyjapc error')
            print(e)
    else:
        print("Error: Gain out of range or non-float")
        status_label.config(text=f"Error: Gain out of range or non-float")

#######################
## Global parameters ##
#######################

image = Image.open('images/Logo_Hearts_cropped.png')
image_resized = image.resize((int(image.size[0]*0.25), int(image.size[1]*0.25)), Image.ANTIALIAS)
bg_image = ImageTk.PhotoImage(image_resized)
background_label = tk.Label(root, image=bg_image)
background_label.grid(row=0, column=1)

### Input Ekin ###
general_frame = tk.Frame(root)
selector_label = tk.Label(general_frame, text="Selector:", font=font_size)
selector_label.grid(row=0, column=0, sticky="e")
selector_entry = tk.Entry(general_frame, font=font_size, width=20)
selector_entry.insert(0, selector_initial)  # Default value
selector_entry.grid(row=0, column=1)

alpha_label = tk.Label(general_frame, text=r"Bending angle [mrad]:", font=font_size)
alpha_label.grid(row=1, column=0)
alpha_entry = tk.Entry(general_frame, font=font_size, width = 6)
alpha_entry.grid(row=1, column=1, sticky="w")
alpha_entry.insert(0, dump_magnet_alpha_initial)
checkbox_var = tk.IntVar(value=1)
checkbox = tk.Checkbutton(general_frame, text="Auto trim Dump magnets", variable=checkbox_var)
checkbox.grid(row=2, column=0, sticky="e")
general_frame.grid(row=0, column = 0)

### Output Bfield ###
output_bfield_frame = tk.Frame(root, relief=tk.SUNKEN, borderwidth=1)
read_button = tk.Button(output_bfield_frame, text="Read B-field", command=read_bfield, font=font_size)
read_button.grid(row=0, column=0, padx=(50,50), pady=(0, 20))
bfield_label = tk.Label(output_bfield_frame, text="Current B-field = ...", font=font_size)
bfield_label.grid(row=1, column=0, columnspan=4, pady=(0, 10), sticky="w")
ekin_label = tk.Label(output_bfield_frame, text="Ekin = ...", font=font_size)
ekin_label.grid(row=2, column=0, columnspan=4, pady=(0, 10), sticky="w")
Brho_label = tk.Label(output_bfield_frame, text="Brho = ...", font=font_size)
Brho_label.grid(row=3, column=0, columnspan=4, pady=(0, 10), sticky="w")
output_bfield_frame.grid(row=0, column = 2)

#####################
## Horizontal line ##
#####################
empty_row = tk.Frame(root, height=20)
empty_row.grid(row=1, column=0)
separator = ttk.Separator(root, orient="horizontal")
separator.grid(row=2, column=0, columnspan=8, sticky="ew")
empty_row = tk.Frame(root, height=20)
empty_row.grid(row=3, column=0)

### Manual set ###
manual_set_frame = tk.Frame(root)
new_ekin_label = tk.Label(manual_set_frame, text=f"New Ekin [GeV/u]:\nRange = [{min_input_ekin} - {max_input_ekin}]", font=font_size + ' bold')
new_ekin_label.grid(row=0, column=0, sticky="e", padx=(0,20))
ekin_entry = tk.Entry(manual_set_frame, font=font_size, width=6)
ekin_entry.grid(row=0, column=1)
manual_set_frame.grid(row=4, column = 0)

set_button = tk.Button(root, text="Set Ekin", command=set_bfield, font=font_size)
set_button.grid(row=4, column=1)
set_magnet_button = tk.Button(root, text="Set Dump Magnets", command=set_magnets, font=font_size)
set_magnet_button.grid(row=4, column=2)

#####################
## Horizontal line ##
#####################
empty_row = tk.Frame(root, height=20)
empty_row.grid(row=5, column=0)
separator = ttk.Separator(root, orient="horizontal")
separator.grid(row=6, column=0, columnspan=8, sticky="ew")
empty_row = tk.Frame(root, height=20)
empty_row.grid(row=7, column=0)

#################
## ENERGY SCAN ##
#################
scan_row = 8
### Scan inputs ###
scan_inputs_frame = tk.Frame(root)
start_ekin_label = tk.Label(scan_inputs_frame, text="Start Ekin [GeV/u]:", font=font_size)
start_ekin_label.grid(row=0, column=0, sticky="e")
start_ekin_entry = tk.Entry(scan_inputs_frame, font=font_size, width=5)
start_ekin_entry.insert(0, start_ekin_initial)  # Default value
start_ekin_entry.grid(row=0, column=1, padx=(0, 20))
end_ekin_label = tk.Label(scan_inputs_frame, text="End Ekin [GeV/u]:", font=font_size)
end_ekin_label.grid(row=0, column=2, sticky="e")
end_ekin_entry = tk.Entry(scan_inputs_frame, font=font_size, width=5)
end_ekin_entry.insert(0, end_ekin_initial)  # Default value
end_ekin_entry.grid(row=0, column=3)
steps_ekin_label = tk.Label(scan_inputs_frame, text="Steps:", font=font_size)
steps_ekin_label.grid(row=1, column=0, sticky="e")
steps_ekin_entry = tk.Entry(scan_inputs_frame, font=font_size, width=5)
steps_ekin_entry.insert(0, steps_ekin_initial)  # Default value
steps_ekin_entry.grid(row=1, column=1, padx=(0, 20))
repetition_ekin_label = tk.Label(scan_inputs_frame, text="Repetition:", font=font_size)
repetition_ekin_label.grid(row=1, column=2, sticky="e")
repetition_ekin_entry = tk.Entry(scan_inputs_frame, font=font_size, width=5)
repetition_ekin_entry.insert(0, repetition_ekin_initial)  # Default value
repetition_ekin_entry.grid(row=1, column=3)
scan_inputs_frame.grid(row=scan_row, column = 0)

### Scan Progress outputs ###
scan_outputs_frame = tk.Frame(root, relief=tk.SUNKEN, borderwidth=1)
status_label = tk.Label(scan_outputs_frame, text="Status: Scan not started", font=font_size)
status_label.grid(row=0, column=0, columnspan=5, sticky="w")
progress_label = tk.Label(scan_outputs_frame, text="Progress: 0%", font=font_size)
progress_label.grid(row=1, column=0, columnspan=2, sticky="w")
progress_bar = Progressbar(scan_outputs_frame, mode="determinate", length=200)
progress_bar.grid(row=1, column=2, columnspan=5)
expected_end_label = tk.Label(scan_outputs_frame, text="Expected End Time:", font=font_size)
expected_end_label.grid(row=2, column=0, columnspan=7, sticky="w")
scan_outputs_frame.grid(row=scan_row, column = 2)

### Scan Buttons ###
scan_button_frame = tk.Frame(root)
start_scan= tk.Button(scan_button_frame, text="Start scan", command=start_subscriptions, font=font_size)
start_scan.grid(row=0, column=0, padx=(0, 50))
stop_scan= tk.Button(scan_button_frame, text="Stop scan", command=stop_subscriptions, font=font_size)
stop_scan.grid(row=0, column=1, padx=(50, 0))
scan_button_frame.grid(row=scan_row + 1, column = 0)

#####################
## Horizontal line ##
#####################
empty_row = tk.Frame(root, height=20)
empty_row.grid(row=scan_row + 2, column=0)
separator = ttk.Separator(root, orient="horizontal")
separator.grid(row=scan_row + 3, column=0, columnspan=8, sticky="ew")
empty_row = tk.Frame(root, height=20)
empty_row.grid(row=scan_row + 4, column=0)

### Gain manual set ###
gain_set_frame = tk.Frame(root)
new_gain_label = tk.Label(gain_set_frame, text=f"New Gain:\nRange = [{min_input_gain} - {max_input_gain}]", font=font_size + ' bold')
new_gain_label.grid(row=0, column=0, sticky="e", padx=(0,20))
gain_entry = tk.Entry(gain_set_frame, font=font_size, width=6)
gain_entry.grid(row=0, column=1)
gain_set_frame.grid(row=scan_row + 5, column = 0)

set_gain_button = tk.Button(root, text="Set Gain", command=set_gain, font=font_size)
set_gain_button.grid(row=scan_row + 5, column=1)

root.mainloop()