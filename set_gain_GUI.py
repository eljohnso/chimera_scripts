import tkinter as tk
import vxi11
import datetime
# import pyjapc

AFG = "128.141.184.139"


# japc = pyjapc.PyJapc(noSet=True)

frev_samples = 152  # Global variable
insert_frev_button = None  # Global variable to hold the insert_frev button

def write_values():
    try:
        instrm = vxi11.Instrument(AFG)
        gain_V = float(gain_entry.get())
        center_freq_kHz = float(center_freq_entry.get())
        fm_dev_kHz = float(fm_dev_entry.get())
        repetition_freq_kHz = float(repetition_freq_entry.get())

        instrm.write(f"SOUR1:VOLT {gain_V} Vpp")
        instrm.write(f"SOUR1:FREQ {center_freq_kHz} kHz")
        instrm.write(f"SOUR1:FM:DEV {fm_dev_kHz} kHz")
        instrm.write("SOUR1:FM:SOUR CH2")
        instrm.write(f"SOUR2:FREQ {repetition_freq_kHz} kHz")
    finally:
        instrm.close()

    # Read values after writing
    read_values()


def read_values():
    try:
        instrm = vxi11.Instrument(AFG)
        gain = instrm.ask("SOUR1:VOLT?")
        center_freq = instrm.ask("SOUR1:FREQ?")
        fm_dev = instrm.ask("SOUR1:FM:DEV?")
        received_frequency = instrm.ask("SOUR2:FREQ?")
        source_fm = instrm.ask("SOUR1:FM:SOUR?")

        gain_V = float(gain)
        center_freq_kHz = float(center_freq) / 1000
        fm_dev_kHz = float(fm_dev) / 1000
        received_frequency_kHz = float(received_frequency) / 1000

        gain_label['text'] = f"Gain = {gain_V} Vpp"
        center_freq_label['text'] = f"Center of the main frequency = {center_freq_kHz} kHz"
        fm_dev_label['text'] = f"Range of the main frequency = {fm_dev_kHz} kHz"
        received_freq_label['text'] = f"Repetition frequency = {received_frequency_kHz} kHz"
        source_fm_label['text'] = f"The source of the deviation is channel {source_fm}"

        # Update window title with current time
        current_time = datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S')
        root.title(f"Values read at {current_time}")
    finally:
        instrm.close()

def get_frev_data():
    global frev_samples
    global insert_frev_button

    selector = selector_entry.get()  # Get the current value of the selector entry
    japc.setSelector(selector)
    frev = japc.getParam("PA.FREV-SD/SamplesAtHotSpots")

    frev_names = frev["names"][1]
    frev_times = frev["times"][1]
    frev_samples = frev["samples"][1]

    frev_label['text'] = f"At {frev_names}\nCtime: {frev_times} ms\nFREV = {round(frev_samples,3)} kHz\nFREV/3 = {round(frev_samples/3,3)}"

    # Create the insert_frev button if it does not already exist
    if insert_frev_button is None:
        insert_frev_button = tk.Button(root, text="Insert FREV", command=insert_frev, font=font_size)
        insert_frev_button.grid(row=6, column=3, columnspan=2)  # adjust the grid position as needed


def insert_frev():
    global insert_frev_button

    # insert the frev_samples value to center_freq_entry
    center_freq_entry.delete(0, 'end')
    center_freq_entry.insert(0, round(frev_samples/3, 3))

    # Destroy the button after it has been used
    insert_frev_button.destroy()
    insert_frev_button = None  # Reset the global variable

root = tk.Tk()
font_size = 'Arial 12'  # You can modify this to increase/decrease the font size


# Set default values
default_gain = 1.0
default_center_freq = 152.75
default_fm_dev = 11.75
default_repetition_freq = 0.1

tk.Label(root, text="Gain (Vpp):", font=font_size).grid(row=0, column=0)
gain_entry = tk.Entry(root)
gain_entry.insert(0, default_gain)
gain_entry.grid(row=0, column=1)
gain_entry.bind('<Return>', lambda event: write_values())
gain_entry.config(font=font_size) # Set font

tk.Label(root, text="Center of the main frequency (kHz):", font=font_size).grid(row=1, column=0)
center_freq_entry = tk.Entry(root)
center_freq_entry.insert(0, default_center_freq)
center_freq_entry.grid(row=1, column=1)
center_freq_entry.bind('<Return>', lambda event: write_values())
center_freq_entry.config(font=font_size) # Set font

tk.Label(root, text="Range of the main frequency (kHz):", font=font_size).grid(row=2, column=0)
fm_dev_entry = tk.Entry(root)
fm_dev_entry.insert(0, default_fm_dev)
fm_dev_entry.grid(row=2, column=1)
fm_dev_entry.bind('<Return>', lambda event: write_values())
fm_dev_entry.config(font=font_size) # Set font

tk.Label(root, text="Repetition frequency (kHz):", font=font_size).grid(row=3, column=0)
repetition_freq_entry = tk.Entry(root)
repetition_freq_entry.insert(0, default_repetition_freq)
repetition_freq_entry.grid(row=3, column=1)
repetition_freq_entry.bind('<Return>', lambda event: write_values())
repetition_freq_entry.config(font=font_size) # Set font

write_button = tk.Button(root, text="Write Values", command=write_values, font=font_size)
write_button.grid(row=4, column=0, columnspan=2)

gain_label = tk.Label(root, text="", font=font_size)
gain_label.grid(row=0, column=2, columnspan=2, sticky='e')
center_freq_label = tk.Label(root, text="", font=font_size)
center_freq_label.grid(row=1, column=2, columnspan=2, sticky='e')
fm_dev_label = tk.Label(root, text="", font=font_size)
fm_dev_label.grid(row=2, column=2, columnspan=2, sticky='e')
received_freq_label = tk.Label(root, text="", font=font_size)
received_freq_label.grid(row=3, column=2, columnspan=2, sticky='e')
source_fm_label = tk.Label(root, text="", font=font_size)
source_fm_label.grid(row=4, column=2, columnspan=2, sticky='e')

read_button = tk.Button(root, text="Read Values", command=read_values, font=font_size)
read_button.grid(row=5, column=2, columnspan=2)


#### FREV #####

selector_label = tk.Label(root, text="Selector:", font=font_size)
selector_label.grid(row=6, column=0)  # change the grid position as needed
selector_entry = tk.Entry(root, font=font_size)
selector_entry.insert(0, "CPS.USER.EAST3")  # Default value
selector_entry.grid(row=6, column=1)  # change the grid position as needed


frev_button = tk.Button(root, text="Get FREV Data", command=get_frev_data, font=font_size)
frev_button.grid(row=6, column=2, columnspan=1)  # change the grid position as needed

frev_label = tk.Label(root, text="", font=font_size, justify=tk.LEFT)
frev_label.grid(row=7, column=2, columnspan=1, sticky='w')  # adjust the grid position as needed

# Read values at the start
read_values()

root.mainloop()
