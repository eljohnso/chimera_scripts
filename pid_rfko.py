import numpy as np
import pyjapc
import pyqtgraph as pg
from PyQt5.QtWidgets import QApplication, QMainWindow, QVBoxLayout, QWidget, QSlider, QLabel
from PyQt5.QtCore import Qt

# Constants and Parameters
BASE_GAIN = 0.5
start_ds = 16
end_ds = 56
skip_points = 4
NUM_GAINS = int((end_ds-start_ds)/skip_points)
GAIN_ARRAY = [BASE_GAIN] * NUM_GAINS
TARGET_SIGNAL = 13

# PID controller parameters
KP = 0.01
KI = 0.005
KD = 0.0

# Initialize PID controller variables
integral = np.zeros(NUM_GAINS)
previous_error = np.zeros(NUM_GAINS)


# Function to initialize and configure the main window
def init_main_window():
    window = QMainWindow()
    window.setWindowTitle('Slider and Plot')
    central_widget = QWidget()
    window.setCentralWidget(central_widget)

    plot_widget = pg.GraphicsLayoutWidget()
    plot1 = plot_widget.addPlot(title="Spill vs. Time")
    plot1.setLabel("bottom", "Time [ms]")
    plot1.setLabel("left", "Intensity (arb.)")
    curve1 = plot1.plot(pen='b')
    curve_xsec23_downsampled = plot1.plot(pen='g', symbol='o')
    infinite_line1 = pg.InfiniteLine(pos=TARGET_SIGNAL, angle=0, movable=False,
                                     pen=pg.mkPen(color="magenta", style=Qt.DashLine))
    curve_smooth = plot1.plot(pen='r')

    plot1.addItem(infinite_line1)

    plot2 = plot_widget.addPlot(title="Array G vs Time")
    plot2.setLabel("bottom", "Time [ms]")
    plot2.setLabel("left", "Array G")
    plot2.setYRange(0, 1)
    curve2 = plot2.plot(pen='r', symbol='x')

    layout = QVBoxLayout()
    layout.addWidget(plot_widget)

    # Add slider for target signal
    slider = QSlider(Qt.Horizontal)
    slider.setRange(0, 100)
    slider.setValue(int(TARGET_SIGNAL))
    slider.valueChanged.connect(update_target_signal)

    # Add label for slider value
    slider_label = QLabel(f"Target Signal: {TARGET_SIGNAL}")

    layout.addWidget(slider_label)
    layout.addWidget(slider)

    central_widget.setLayout(layout)

    return window, curve1, curve_xsec23_downsampled, curve_smooth, curve2, infinite_line1, slider_label


# Function to update target signal based on slider value
def update_target_signal(value):
    global TARGET_SIGNAL, infinite_line1
    TARGET_SIGNAL = value
    infinite_line1.setPos(TARGET_SIGNAL)
    slider_label.setText(f"Target Signal: {TARGET_SIGNAL}")

def moving_average(signal, window_size):
    return np.convolve(signal, np.ones(window_size)/window_size, mode='valid')

# Callback function for JAPC parameter updates
def my_callback(parameter_name, new_value):
    global integral, previous_error, GAIN_ARRAY

    try:
        # Extract data from JAPC
        signal_23_I1 = -japc.getParam("F61.XSEC023-I1/SpillData#semSpillData")
        integration_duration = japc.getParam("F61.XSEC023-I1/Acquisition#integrationDuration")
        nb_of_samples = japc.getParam("F61.XSEC023-I1/Acquisition#nbOfSamples")

        start_xsec23_acqu_time = 700
        t = np.arange(0, integration_duration, integration_duration / nb_of_samples) + start_xsec23_acqu_time
        signal_xsec23 = signal_23_I1

        # Moving average
        window_size = 5
        smoothed_signal = moving_average(signal_xsec23, window_size)
        #curve_smooth.setData(t[:len(smoothed_signal)], smoothed_signal)


        # Update the first plot
        curve1.setData(t, signal_xsec23)
        curve_xsec23_downsampled.setData(t[start_ds:end_ds:skip_points], signal_xsec23[start_ds:end_ds:skip_points])
        #print(f'time: {t[start_ds:end_ds:2]}')



        # PID control calculations
        #arr_t = [0, 1100, 1200, 1300, 1400, 1500, 1600, 1700, 1800,  1900, 2300]
        arr_t = t[start_ds:end_ds:skip_points]
        arr_t = np.insert(arr_t, 0, 0) # We need to insert a zero point to start the function
        arr_t = np.insert(arr_t, 1, 900)  # We need to insert a zero point to start the function
        print(arr_t)
        target_xsec23 = np.array([TARGET_SIGNAL] * len(signal_xsec23[start_ds:end_ds:skip_points]))
        error = target_xsec23 - signal_xsec23[start_ds:end_ds:skip_points]

        P = KP * error
        integral += error
        I = KI * integral
        derivative = error - previous_error
        D = KD * derivative
        pid_output = P + I + D
        previous_error = error

        GAIN_ARRAY = pid_output.clip(min=0, max=1).tolist()
        GAIN_ARRAY = np.insert(GAIN_ARRAY, 0, 0)
        GAIN_ARRAY = np.insert(GAIN_ARRAY, 1, 0)
        print(f'Gain Array {GAIN_ARRAY}')

        # Send updated gain to JAPC
        japc.setParam('PR.BQL72/Setting#exAmplitudeH',1)
        japc.setParam('PR.BQL72/FunctionSetting#exScaling', np.array([arr_t, GAIN_ARRAY]))

        # Update the second plot
        curve2.setData(arr_t, GAIN_ARRAY)

    except Exception as e:
        print(f"Error in my_callback: {e}")


# Main application initialization
if __name__ == "__main__":
    app = QApplication([])

    window, curve1, curve_xsec23_downsampled, curve_smooth, curve2, infinite_line1, slider_label = init_main_window()

    japc = pyjapc.PyJapc(noSet=False)
    japc.setSelector("CPS.USER.EAST4")

    japc.subscribeParam("F61.XSEC023-I1/SpillData#semSpillData", my_callback)
    japc.startSubscriptions()

    window.show()
    app.exec_()
