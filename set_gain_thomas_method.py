import vxi11
AFG = "128.141.184.20"
instrm = vxi11.Instrument(AFG)


VOLT = 1.0 # Max 2 Volts
# instrm.write(f"SOUR1:VOLT {VOLT} Vpp") # Gain
# instrm.write("SOUR1:FREQ 152.75 kHz") # Center of the main frequency
# instrm.write("SOUR1:FM:DEV 11.75 kHz") # Range of the main frequency
# instrm.write("SOUR1:FM:SOUR CH2") # The source of the deviation is channel 2
#
# initial_parameter = param_space[0]
# initial_freq = 1/(initial_parameter/1000)
# print(f"> Initial parameter: {initial_freq} Hz / {initial_parameter} ms")
# instrm.write(f"SOUR2:FREQ {initial_freq} Hz") # Repetition frequency



### READING

gain = instrm.ask("SOUR1:VOLT?")
center_freq = instrm.ask("SOUR1:FREQ?")
fm_dev = instrm.ask("SOUR1:FM:DEV?")
received_frequency = instrm.ask("SOUR2:FREQ?")
source_fm = instrm.ask("SOUR1:FM:SOUR?")

# Convert frequencies from string to float and then to kHz
gain_V = float(gain)
center_freq_kHz = float(center_freq) / 1000
fm_dev_kHz = float(fm_dev) / 1000
received_frequency_kHz = float(received_frequency) / 1000

print(f"Gain = {gain_V} Vpp")
print(f"Center of the main frequency = {center_freq_kHz} kHz")
print(f"Range of the main frequency = {fm_dev_kHz} kHz")
print(f"Repetition frequency = {received_frequency_kHz} kHz")
print(f"The source of the deviation is channel {source_fm}")
