import pyjapc
import time
import vxi11
import numpy as np
import pyqtgraph as pg
from PyQt5.QtWidgets import QApplication, QMainWindow, QLabel, QSlider, QVBoxLayout, QWidget
from PyQt5 import QtGui, QtWidgets, QtCore



AFG = "128.141.184.20"
instrm = vxi11.Instrument(AFG)

japc = pyjapc.PyJapc(noSet=True)
japc.setSelector("CPS.USER.EAST3")

# Lists to store time and gain values for plotting
time_values = []
gain_values = []
time_values_xsec23 = []
signal_xsec23 = []
shot_number_list = []
shot_number = 0
delta_target_error_list = []
individual_gain_list = []
individual_target_list = []

base_gain = 1
number_of_gain_to_send = 9
gain_array = [base_gain]*number_of_gain_to_send
target_xsec23_signal = 30000

# PID control parameters (you may need to tune these)
Kp = 0.0001  # Proportional gain
Ki = 0.00001  # Integral gain
Kd = 0.0  # Derivative gain

integral = [0]*number_of_gain_to_send  # Integral term accumulator
prev_error = [0]*number_of_gain_to_send  # Previous error for derivative term

def onSliderValueChanged(value):
    global target_xsec23_signal
    target_xsec23_signal = value

# Create an application and a plot window
app = QApplication([])

# Create the main window
window = QMainWindow()
window.setWindowTitle('Slider and Plot')
central_widget = QWidget()
window.setCentralWidget(central_widget)

# Create a vertical layout for the central widget
layout = QVBoxLayout()

# Create a label
label = QLabel('Target XSEC23 Signal:')
layout.addWidget(label)

# Create a slider to adjust the target signal value
slider = QSlider(QtCore.Qt.Horizontal)
slider.setMinimum(0)
slider.setMaximum(50000)  # Adjust the maximum value as needed
slider.setValue(8000)  # Initial value
slider.valueChanged.connect(onSliderValueChanged)
layout.addWidget(slider)


# Create a PyqtGraph plot widget
plot_widget = pg.GraphicsLayoutWidget()

plot = plot_widget.addPlot(title="Gain vs. Time")
plot.setLabel("bottom", "Time [ms]")
plot.setLabel("left", "Gain (Vpp)")
curve = plot.plot(pen='b', symbol='o')

# Create the second plot
plot2 = pg.PlotItem(title="Target signal")
plot2.setLabel("bottom", "Time [ms]")
plot2.setLabel("left", "XSEC23 []")
plot_widget.addItem(plot2)
curve_xsec23 = plot2.plot(pen='r', symbol='x')
curve_xsec23_downsampled = plot2.plot(pen='g', symbol='o')

plot_widget.nextRow()

# Create two additional plots
plot3 = plot_widget.addPlot(title="Error")
plot3.setLabel("bottom", "Shot Number")
plot3.setLabel("left", "XSEC23 []")
curve3 = plot3.plot(pen='y', symbol='s')

plot4 = plot_widget.addPlot(title="Individual Gain Evolution")
plot4.setLabel("bottom", "Shot number")
plot4.setLabel("left", "Gain (Vpp)")

# Infinite lines
infinite_line1 = pg.InfiniteLine(pos=base_gain, angle=0, movable=False,
                                pen=pg.mkPen(color="magenta", style=QtCore.Qt.DashLine))
plot.addItem(infinite_line1)
infinite_line2 = pg.InfiniteLine(pos=target_xsec23_signal, angle=0, movable=False,
                                pen=pg.mkPen(color="orange", style=QtCore.Qt.DashLine))
plot2.addItem(infinite_line2)
infinite_line3 = pg.InfiniteLine(pos=target_xsec23_signal, angle=0, movable=False,
                                pen=pg.mkPen(color="orange", style=QtCore.Qt.DashLine))
plot3.addItem(infinite_line3)

curve_items = []
legend = plot4.addLegend()
for i in range(number_of_gain_to_send):
    curve_i = plot4.plot(pen=pg.mkPen(color=i, width=2), name=f"Gain {i + 1}")
    curve_items.append(curve_i)

curve_items_target = []
legend = plot3.addLegend()
for i in range(number_of_gain_to_send):
    curve_i = plot3.plot(pen=pg.mkPen(color=i, width=2), name=f"signal {i + 1}")
    curve_items_target.append(curve_i)

# Set the layout for the central widget
layout.addWidget(plot_widget)
central_widget.setLayout(layout)

def update_plot():
    global gain_array
    global Kp, Ki, Kd, integral, prev_error
    global time_values, gain_values, time_values_xsec23, signal_xsec23
    try:
        curve.setData(time_values, gain_values)
        for i in range(len(curve_items)): # Print all the individual gain settings
            curve_items[i].setData(shot_number_list, np.array(individual_gain_list)[:, i])
        if ( (len(time_values_xsec23) == 42) and (len(signal_xsec23) == 42) ):
            curve_xsec23.setData(time_values_xsec23, signal_xsec23)
            curve_xsec23_downsampled.setData(time_values_xsec23[16:34:2], signal_xsec23[16:34:2])
        for i in range(len(curve_items_target)): # Print all the individual target
            curve_items_target[i].setData(shot_number_list, np.array(individual_target_list)[:, i])

        infinite_line2.setValue(target_xsec23_signal)
        infinite_line3.setValue(target_xsec23_signal)

    except Exception as e:
        # Handle any other exception (general catch-all)
        print(f"An error occurred: {e}")
        pass

def myCallback(parameterName, newValue):
    print(f"New value for {parameterName} is: {newValue}")

    # Perhaps here I need to wait a few ms to delay the sequence

    start_callback_time = time.time()
    global time_values, gain_values, time_values_xsec23, signal_xsec23
    global shot_number, shot_number_list, delta_target_error_list, individual_gain_list, individual_target_list
    time_values = []
    gain_values = []
    time_values_xsec23 = []
    signal_xsec23 = []

    time_between_gain_change = 0.04 #s you can go down to 30 ms

    iteration = 0

    # Set the initial gain
    VOLT = gain_array[0]
    instrm.write(f"SOUR1:VOLT {VOLT} Vpp")  # Gain

    while iteration < len(gain_array):
        start_time = time.time()

        ################################
        ### GAIN and DATA processing ###
        ################################
        # Set the gain
        VOLT = gain_array[iteration]
        instrm.write(f"SOUR1:VOLT {VOLT} Vpp")  # Gain

        # Read the gain
        gain = float(instrm.ask("SOUR1:VOLT?"))
        # print(f"Gain = {gain} Vpp")

        # Append time and gain values to lists
        time_values.append( (time.time()-start_callback_time) * 1000 ) # Append time difference in ms
        gain_values.append(gain)

        #################################################################################
        # Waiting the appropriate time before sending the next signal generator command #
        #################################################################################
        iteration += 1
        time_to_sleep = time_between_gain_change - (time.time() - start_time)
        if time_to_sleep >= 0.0:
            time.sleep(time_to_sleep)
            # print(f"You had {time_to_sleep:.4f} extra second for computations")
        if time_to_sleep < 0.0:
            print(f"Alert, computation time is too long by {time_to_sleep:.4f} !")

    # XSEC23 data
    signal_23_I1 = -japc.getParam("F61.XSEC023-I1/SpillData#semSpillData")
    integrationDuration = japc.getParam("F61.XSEC023-I1/Acquisition#integrationDuration")
    nbOfSamples = japc.getParam("F61.XSEC023-I1/Acquisition#nbOfSamples")
    t = np.arange(0, integrationDuration, int(integrationDuration / nbOfSamples))  # time in ms
    time_values_xsec23 = t
    signal_xsec23 = signal_23_I1

    if (len(t == 42) and len(signal_xsec23) == 42):

        shot_number_list.append(shot_number)
        shot_number += 1

        ## Compute the error
        target_xsec23 = [target_xsec23_signal]*number_of_gain_to_send
        error = target_xsec23 - signal_xsec23[16:34:2]
        delta_target_error_list.append(error.sum())
        individual_target_list.append(signal_xsec23[16:34:2])

        ## Set the new gain_array
        for i in range(len(gain_array)):
            gain_array[i] = gain_array[i] + Kp*error[i] + Ki*integral[i] + Kd*(error[i]-prev_error[i])
            if gain_array[i] < 0:
                gain_array[i] = 0 # Prevents having negative values being set for the gain
            integral[i] += error[i]
            prev_error[i] = error[i]
        individual_gain_list.append(gain_array.copy())

    print("Waiting for the next shot")
    print("")


# Start the Qt application's event loop
timer = QtCore.QTimer()
timer.timeout.connect(update_plot)
timer.start(100)  # Update the plot every x milliseconds

window.show()

# Start the subscription
japc.subscribeParam("PE2X.F900-CTML/Acquisition#acqC", myCallback)
japc.startSubscriptions()