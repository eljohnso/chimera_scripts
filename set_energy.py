import pyjapc
from madxtools.transfer_function import *

japc = pyjapc.PyJapc(noSet=True)
selector = "CPS.USER.EAST3"
japc.setSelector(selector)
# japc.rbacLogin()

bfield = japc.getParam("PR.MPC/REF.PPPL#REF4")
print(f'Actual B-field at flattop = {bfield[0]} [G]')

new_bfield = 2621
bfield[0] = new_bfield
print(f'New B-field = {bfield[0]} [G]')
japc.setParam("PR.MPC/REF.PPPL#REF4", bfield, checkDims=False)

# Change the bending magnet
# dump_bend = -66 * new_bfield / 2581
# # dump_bend = -34

# alpha = angle(600, "MCB", 80.054)

alpha = 0.0485501189621215

japc.setSelector(selector)
bfield = japc.getParam("PR.MPC/REF.PPPL#REF4")

rho = 70.0789
Brho = rho * bfield[0] / 10000

print(f"Brho = {round(Brho,3)}")

dump_bend = get_current_dipole(alpha, "MCB", Brho)


japc.setParam("F61.BHZ01.DUMP.A/REF.PULSE.AMPLITUDE#VALUE", dump_bend)
japc.setParam("F61.BHZ01.DUMP.B/REF.PULSE.AMPLITUDE#VALUE", dump_bend)
print(f'Setting dump bend current to = {dump_bend} [A]')

import sys
sys.exit()