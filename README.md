# CHIMERA Scripts

## Energy GUI

To be used on the CCC computers.

<img src="images/energy_gui.png" width="600">

## Set Gain

Can be used on any machine connected to the CERN network. The Get FREV data button only work in a Technical Network virtual machine.

<img src="images/set_gain_GUI.png" width="600">


## Set Optics (small, large beam size)
