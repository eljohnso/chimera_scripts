import pyjapc
import numpy as np
from madxtools.transfer_function import *


japc = pyjapc.PyJapc(noSet=False)
selector = "CPS.USER.MD3"
japc.setSelector(selector)
japc.rbacLogin()


ekin_array = np.linspace(1.4, 1.7, 5)

plateau_number = 0
second_plateau_index = 1
def set_bfield(E_cin_per_nucleon):
    # E_cin_per_nucleon = float(ekin_entry.get())

    if validate_float(E_cin_per_nucleon):

        # Ion properties
        A = 208.0
        Z = 82.0
        N = 126.0
        charge = 54.0
        m_proton_GeV = 0.93828
        m_neutron_GeV = 0.93957
        m_electron_GeV = 0.000511
        m_u_GeV = 0.9315
        mass_defect_GeV = Z * m_proton_GeV + N * m_neutron_GeV + (Z - charge) * m_electron_GeV - A * m_u_GeV
        E_0 = Z * m_proton_GeV + N * m_neutron_GeV - mass_defect_GeV
        p = E_0 * np.sqrt((((E_cin_per_nucleon * A) / E_0) + 1) ** 2 - 1)
        rho = 70.0789
        B = 3.3356 * p / (rho * charge) * 10000

        new_bfield = B
        print(f"Setting to {round(new_bfield,1)} [G]")

        japc.setSelector(selector)
        bfield = japc.getParam("PR.MPC/REF.PPPL")
        bfield["REF4"][plateau_number] = new_bfield

        try:
            if new_bfield < 4250:
                bfield["ACCELERATION1"][second_plateau_index] = 300000
                bfield["ACCELERATION3"][second_plateau_index] = -300000
                bfield["RATE2"][second_plateau_index] = 22000
                japc.setParam("PR.MPC/REF.PPPL", bfield, checkDims=False)
            if new_bfield >= 4250:
                bfield["ACCELERATION1"][second_plateau_index] = -300000
                bfield["ACCELERATION3"][second_plateau_index] = 300000
                bfield["RATE2"][second_plateau_index] = -22000
                japc.setParam("PR.MPC/REF.PPPL", bfield, checkDims=False)
        except Exception as e:
            print('Ignored pyjapc error')
            print(e)

    else:
        print("Error: Ekin out of range or non-float")

def set_magnets(alpha):
    # selector = selector_entry.get()

    japc.setSelector(selector)
    bfield = japc.getParam("PR.MPC/REF.PPPL#REF4")

    rho = 70.0789
    Brho = rho*bfield[plateau_number]/10000

    dump_bend = -get_current_dipole(alpha, "MCB", Brho)*54/82

    japc.setParam("F61.BHZ01.DUMP.A/REF.PULSE.AMPLITUDE#VALUE", dump_bend)
    japc.setParam("F61.BHZ01.DUMP.B/REF.PULSE.AMPLITUDE#VALUE", dump_bend)

    print(f'Setting dump bend current to = {round(dump_bend,3)} [A]')

def validate_float(value):
    try:
        float_value = float(value)
        return 0.4 <= float_value <= 2.7
    except ValueError:
        return False


#Loop
i=0
def myCallback(parameterName, newValue):

    global i

    print(f"Setting Ekin to {round(ekin_array[i],3)} [GeV/u]")

    set_bfield(ekin_array[i])
    set_magnets(0.047) # Dump magnets
    print("Done")

    i+=1
    if i==len(ekin_array):
        i-=1
        print("SCAN COMPLETED")


japc.subscribeParam("PR.BCT/HotspotIntensity#dcBefEje2", myCallback)
japc.startSubscriptions()