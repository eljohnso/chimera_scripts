import time
import numpy as np
import vxi11
AFG = "128.141.184.20"
instrm = vxi11.Instrument(AFG)

# Function to poll the signal generator and get values
def poll_signal_generator():
    instrm = vxi11.Instrument(AFG)
    gain = instrm.ask("SOUR1:VOLT?")
    center_freq = instrm.ask("SOUR1:FREQ?")
    fm_dev = instrm.ask("SOUR1:FM:DEV?")
    received_frequency = instrm.ask("SOUR2:FREQ?")
    source_fm = instrm.ask("SOUR1:FM:SOUR?")

    gain_V = float(gain)
    center_freq_kHz = float(center_freq) / 1000
    fm_dev_kHz = float(fm_dev) / 1000
    received_frequency_kHz = float(received_frequency) / 1000

    return gain_V, center_freq_kHz, fm_dev_kHz, received_frequency_kHz

# Main loop to poll and log values
while True:
    try:
        # Poll the signal generator
        gain_V, center_freq_kHz, fm_dev_kHz, received_frequency_kHz = poll_signal_generator()

        # Log the value to a file (append mode)
        with open('rfko_signal_gen_log.txt', 'a') as log_file:
            log_file.write(f'{time.strftime("%Y-%m-%d %H:%M:%S")}, {gain_V}, {center_freq_kHz}, {fm_dev_kHz}, {received_frequency_kHz}\n')

        print(f"Gain = {gain_V} Vpp")
        print(f"Center of the main frequency = {center_freq_kHz} kHz")
        print(f"Range of the main frequency = {fm_dev_kHz} kHz")
        print(f"Repetition frequency = {received_frequency_kHz} kHz")

        # Sleep for 30 seconds before polling again
        time.sleep(30)
    except KeyboardInterrupt:
        print('Polling stopped by user.')
        break
    except Exception as e:
        print(f'Error: {e}')
