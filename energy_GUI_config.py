window_title = "CHIMERA/HEARTS Energy GUI"
font_size = 'Arial 13'

plateau_number = 0 # Plateau where the flattop B-field is set
second_plateau_index = 1 # Plateau where the 4250 G is set

min_input_ekin = 0.65
max_input_ekin = 2.7

min_input_gain = 0.04
max_input_gain = 0.3

selector_initial = "CPS.USER.MD3"
dump_magnet_alpha_initial = 0.047
start_ekin_initial = 0.65
end_ekin_initial = 2.7
steps_ekin_initial = 3
repetition_ekin_initial = 1