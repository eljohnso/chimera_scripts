import pyjapc
import numpy as np
from madxtools.transfer_function import *
import tkinter as tk
from tkinter.ttk import Progressbar
import itertools
import datetime

japc = pyjapc.PyJapc(noSet=True)
selector = "CPS.USER.EAST2"
japc.setSelector(selector)
# japc.rbacLogin()

root = tk.Tk()
font_size = 'Arial 12'  # You can modify this to increase/decrease the font size
root.title(f"Energy scan GUI")

plateau_number = 0
second_plateau_index = 1
def set_bfield(E_cin_per_nucleon):
    # E_cin_per_nucleon = float(ekin_entry.get())

    if validate_float(E_cin_per_nucleon):
        # Ion properties
        A = 208.0
        Z = 82.0
        N = 126.0
        charge = 54.0
        m_proton_GeV = 0.93828
        m_neutron_GeV = 0.93957
        m_electron_GeV = 0.000511
        m_u_GeV = 0.9315
        mass_defect_GeV = Z * m_proton_GeV + N * m_neutron_GeV + (Z - charge) * m_electron_GeV - A * m_u_GeV
        E_0 = Z * m_proton_GeV + N * m_neutron_GeV - mass_defect_GeV
        p = E_0 * np.sqrt((((E_cin_per_nucleon * A) / E_0) + 1) ** 2 - 1)
        rho = 70.0789
        B = 3.3356 * p / (rho * charge) * 10000

        new_bfield = B
        print(f"Setting to {round(new_bfield,1)} [G]")

        japc.setSelector(selector)
        bfield = japc.getParam("PR.MPC/REF.PPPL")
        bfield["REF4"][plateau_number] = new_bfield

        try:
            if new_bfield < 4250:
                bfield["ACCELERATION1"][second_plateau_index] = 300000
                bfield["ACCELERATION3"][second_plateau_index] = -300000
                bfield["RATE2"][second_plateau_index] = 22000
                japc.setParam("PR.MPC/REF.PPPL", bfield, checkDims=False)
            if new_bfield >= 4250:
                bfield["ACCELERATION1"][second_plateau_index] = -300000
                bfield["ACCELERATION3"][second_plateau_index] = 300000
                bfield["RATE2"][second_plateau_index] = -22000
                japc.setParam("PR.MPC/REF.PPPL", bfield, checkDims=False)
        except Exception as e:
            print('Ignored pyjapc error')
            print(e)

    else:
        print("Error: Ekin out of range or non-float")

def set_magnets(alpha):
    # selector = selector_entry.get()

    japc.setSelector(selector)
    bfield = japc.getParam("PR.MPC/REF.PPPL#REF4")

    rho = 70.0789
    Brho = rho*bfield[plateau_number]/10000

    dump_bend = -get_current_dipole(alpha, "MCB", Brho)*54/82

    japc.setParam("F61.BHZ01.DUMP.A/REF.PULSE.AMPLITUDE#VALUE", dump_bend)
    japc.setParam("F61.BHZ01.DUMP.B/REF.PULSE.AMPLITUDE#VALUE", dump_bend)

    print(f'Setting dump bend current to = {round(dump_bend,3)} [A]')

def validate_float(value):
    try:
        float_value = float(value)
        return 0.4 <= float_value <= 2.7
    except ValueError:
        return False


#Loop
i=0
ekin_array = None
def myCallback(parameterName, newValue):

    global i
    global ekin_array
    global started_subscription
    global start_scan_time

    progress = (i + 1) / len(ekin_array) * 100
    progress_label.config(text=f"Progress: {int(progress)}%")
    status_label.config(text=f"Status: Scan in progress")
    progress_bar["value"] = progress

    # Estimate the time between callbacks and calculate expected end time
    if i > 0:
        time_elapsed = datetime.datetime.now() - start_scan_time
        expected_total_time = time_elapsed / (i / len(ekin_array))
        expected_end_time = start_scan_time + expected_total_time
        expected_end_time_str = expected_end_time.strftime("%H:%M:%S")
        expected_end_label.config(text=f"Expected End Time: {expected_end_time_str}")

    print(f"Setting Ekin to {round(ekin_array[i],3)} [GeV/u]")
    print(f"Scan {i}/{len(ekin_array)}")

    set_bfield(ekin_array[i])
    set_magnets(0.047) # Dump magnets
    print("Done")

    i+=1
    if i==len(ekin_array):
        stop_subscriptions()

started_subscription = False
start_scan_time = None
def start_subscriptions():
    global ekin_array
    global started_subscription
    global start_scan_time
    if started_subscription == False:
        started_subscription = True
        start_scan_time = datetime.datetime.now()
        status_label.config(text=f"Scan in progress")
        expected_end_label.config(text=f"Expected End Time: Calculating...")
        ekin_array = np.linspace(float(start_ekin_entry.get()), float(end_ekin_entry.get()), int(steps_ekin_entry.get()))
        ekin_array = list(itertools.chain.from_iterable(itertools.repeat(x, int(repetition_ekin_entry.get())) for x in ekin_array))
        japc.subscribeParam("PR.BCT/HotspotIntensity#dcBefEje2", myCallback)
        japc.startSubscriptions()
        print('Started subscription')
    else:
        print("Can start another subscription")
        status_label.config(text=f"Error: Can start another subscription")

def stop_subscriptions():
    global i
    global started_subscription
    global ekin_array
    if (i > 0) and (i < len(ekin_array)): # Before the scan finished
        i = 0
        status_label.config(text=f"Status: Scan Cancelled")
        progress = (i) / len(ekin_array) * 100
        progress_label.config(text=f"Progress: {int(progress)}%")
        progress_bar["value"] = progress
        expected_end_label.config(text=f"Expected End Time:")
    if (i > 0) and (i == len(ekin_array)):
        status_label.config(text=f"Status: Scan Completed")
        progress = (i) / len(ekin_array) * 100
        progress_label.config(text=f"Progress: {int(progress)}%")
        progress_bar["value"] = progress
        expected_end_label.config(text=f"Expected End Time: ")
    else:
        status_label.config(text=f"Status: Scan Cancelled")
    i = 0
    print("SCAN COMPLETED")
    japc.stopSubscriptions()
    japc.clearSubscriptions()
    started_subscription = False

start_ekin_label = tk.Label(root, text="Start Ekin GeV/u:", font=font_size)
start_ekin_label.grid(row=0, column=0)
start_ekin_entry = tk.Entry(root, font=font_size, width=5)
start_ekin_entry.insert(0, 0.5)  # Default value
start_ekin_entry.grid(row=0, column=1)

end_ekin_label = tk.Label(root, text="End Ekin GeV/u:", font=font_size)
end_ekin_label.grid(row=0, column=2)
end_ekin_entry = tk.Entry(root, font=font_size, width=5)
end_ekin_entry.insert(0, 1.0)  # Default value
end_ekin_entry.grid(row=0, column=3)

steps_ekin_label = tk.Label(root, text="Steps:", font=font_size)
steps_ekin_label.grid(row=0, column=4)
steps_ekin_entry = tk.Entry(root, font=font_size, width=5)
steps_ekin_entry.insert(0, 3)  # Default value
steps_ekin_entry.grid(row=0, column=5)

repetition_ekin_label = tk.Label(root, text="Repetition:", font=font_size)
repetition_ekin_label.grid(row=0, column=6)
repetition_ekin_entry = tk.Entry(root, font=font_size, width=5)
repetition_ekin_entry.insert(0, 1)  # Default value
repetition_ekin_entry.grid(row=0, column=7)

progress_label = tk.Label(root, text="Progress: 0%", font=font_size)
progress_label.grid(row=1, column=0, columnspan=2)

progress_bar = Progressbar(root, mode="determinate", length=200)
progress_bar.grid(row=1, column=1, columnspan=5)

# Row 2
expected_end_label = tk.Label(root, text="Expected End Time:", font=font_size)
expected_end_label.grid(row=2, column=0, columnspan=5)

# Row 3
start_scan= tk.Button(root, text="Start scan", command=start_subscriptions, font=font_size)
start_scan.grid(row=3, column=0)

status_label = tk.Label(root, text="Status: Scan not started", font=font_size)
status_label.grid(row=3, column=1, columnspan=5)

stop_scan= tk.Button(root, text="Stop scan", command=stop_subscriptions, font=font_size)
stop_scan.grid(row=3, column=6)

root.mainloop()

