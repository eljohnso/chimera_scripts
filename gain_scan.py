import pyjapc
import numpy as np

japc = pyjapc.PyJapc(noSet=False)
japc.setSelector("CPS.USER.EAST4")
japc.rbacLogin()


gain_array = [0.04, 0.045, 0.05, 0.055, 0.06, 0.07, 0.08, 0.09, 0.10, 0.11, 0.12, 0.13, 0.14, 0.15, 0.16, 0.17, 0.18, 0.19, 0.20]


i=0
def myCallback(parameterName, newValue):

    global i

    gain = gain_array[i]

    try:
        japc.setParam("PR.BQL72/Setting#exAmplitudeH", gain)
        print(f"Gain = {gain}")
    except:
        print('Error ignored')

    i+=1
    if i==len(gain_array):
        i-=1
        print("SCAN COMPLETED")


japc.subscribeParam("PR.BCT/HotspotIntensity#dcBefEje2", myCallback)
japc.startSubscriptions()