import numpy as np
import pyjapc
import datetime

japc = pyjapc.PyJapc(noSet=False)
japc.rbacLogin()
user = "CPS.USER.MD3"
japc.setSelector(user)

bct = "PR.BCT/HotspotIntensity#dcAftInj1"
bct_threshold = 0.5

energy = 2.0
fluence_sum = 0
fluence_threshold = 8e6

print("")
print(f"Fluence threshold set to "+'{:.2e}'.format(fluence_threshold))
print(f"Energy should be {energy} [GeV/u]")

def stop_beam():
    print("Stopping Beam")
    japc.setParam("EC.116-CTML/SoftCondition#softCondition", True, timingSelectorOverride="")  # True stops the beam
    print("Stopping subscriptions")
    japc.stopSubscriptions()
    return

plot_enable = True

if plot_enable:
    import pyqtgraph as pg
    from pyqtgraph.Qt import QtGui, QtCore
    from PyQt5 import QtWidgets
    app = QtWidgets.QApplication([])
    win = pg.GraphicsLayoutWidget(title="Fluence Stop")
    win.show()
    plot = win.addPlot(title="Fluence")
    plot.setLabel("bottom", 'Shot')
    plot.setLabel("left", "Fluence")
    curve = plot.plot(pen='b', symbol = 'o')
    infinite_line = pg.InfiniteLine(pos=fluence_threshold, angle=0, movable=False, pen=pg.mkPen(color='magenta', style=QtCore.Qt.DashLine))
    plot.addItem(infinite_line)


datetime_list = []
fluence_list = []

def myCallback(parameterName, newValue, header):
    global fluence_sum
    bct_intensity = japc.getParam(bct)     # Check if BCT in the PSB is above threshold
    # try:
    if bct_intensity > bct_threshold:
        print("")
        print('New shot')

        xsec70_raw = abs(japc.getParam("T08.XSEC070-I/Acquisition#semRawData"))
        xsec70_gain = japc.getParam("T08.XSEC070-I/GainSetting#ampGain")

        # print(xsec70_gain[1]) # debug
        if xsec70_gain[1] == 'LOW':
            gain = 1
        elif xsec70_gain[1] == 'MID':
            gain = 20
        elif xsec70_gain[1] == 'HIGH':
            gain = 400
        # print(gain) # debug

        print(f"xsec raw = {xsec70_raw}")
        print(f"xsec gain = {gain}")

        if energy == 2:
            fluence = xsec70_raw / gain * 289
        if energy == 0.75:
            fluence = xsec70_raw / gain * 107

        fluence_sum += fluence
        print(fluence_sum)

        if plot_enable:
            datetime_list.append(datetime.datetime.now())
            fluence_list.append(fluence_sum)
            curve.setData(fluence_list)

        if fluence_sum > fluence_threshold:
            print("Fluence above threshold")
            stop_beam()
    else:
        print(f"Skipping on low intensity {round(bct_intensity,3)} charges")
    # except Exception as e:
    #     print("Caught exception")
    #     print(e)
    #     # stop_beam()

print("Subscribing")
japc.subscribeParam("T08.XSEC070-I/Acquisition", myCallback, getHeader=True)
japc.startSubscriptions()