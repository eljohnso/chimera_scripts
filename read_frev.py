import pyjapc

japc = pyjapc.PyJapc(noSet=True)
japc.setSelector("CPS.USER.EAST3")

frev = japc.getParam("PA.FREV-SD/SamplesAtHotSpots")

print(frev["names"][1])
print(frev["times"][1])
print(frev["samples"][1])